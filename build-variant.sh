#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

arch=$1 # hardware architecture or board
tag=$2  # Python version

IFS=_ read -ra _tagparts <<< ${tag}

ros_tag=${_tagparts[0]}
numpy_version=${_tagparts[1]}

sed \
	-e "s@#{ARCH}@${arch}@g" \
	-e "s@#{ROS_TAG}@${ros_tag}@g" \
	-e "s@#{NUMPY_VERSION}@${numpy_version}@g"\
	Dockerfile.tpl > Dockerfile.${tag}

docker build --pull \
	-f Dockerfile.${tag} -t ${arch}-ros-numpy:${tag} \
	$(dirname $0)

# TODO: Make optional
rm Dockerfile.${tag}
